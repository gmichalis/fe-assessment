import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Checkbox } from 'antd';
import { COLUMN_VALUE_LABEL, COLUMNS } from '@constants';

import { modifySelectedColumns } from '@actions/user';

import './ColumnSelector.css';

const ColumnSelector = () => {
  const selectedColumns = useSelector(state => state.selectedColumns);
  const [isIndeterminate, setIndeterminateStatus] = useState(false);
  const [isAllChecked, setAllCheckStatus] = useState(true);

  const dispatch = useDispatch();

  useEffect(() => {
    // new values after modification of selectedColumns
    const updatedIndeterminateValue = !!selectedColumns.length && selectedColumns.length < COLUMNS.length;
    const updatedCheckedAllValue = selectedColumns.length === COLUMNS.length;
    
    setIndeterminateStatus(updatedIndeterminateValue);  
    setAllCheckStatus(updatedCheckedAllValue);
  }, [selectedColumns, COLUMNS]);

  const onChange = checkedList => dispatch(modifySelectedColumns(checkedList));

  const onCheckAllChange = ({ target: { checked } })=> {
    const columns = checked ? COLUMNS : [];

    dispatch(
      modifySelectedColumns(columns)
    );
  };

  return (
    <div className="column-selector">
      <div className="column-selector__header">
        <span className="header__title">Columns</span>
        <Checkbox
          className="header__option"
          indeterminate={isIndeterminate}
          onChange={onCheckAllChange}
          checked={isAllChecked}
        >
          Select all
        </Checkbox>
      </div>
      <Checkbox.Group
        className="column-selector__content"
        options={COLUMN_VALUE_LABEL}
        value={selectedColumns}
        onChange={onChange}
      />
    </div>
  );
}

export default ColumnSelector;