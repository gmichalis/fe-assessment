import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { shallowEqual, useSelector, useDispatch } from 'react-redux';
import { Pagination as AntPagination } from 'antd';

import { fetchForPage } from '@actions/user';
import { RESULT_LIMIT } from '@constants'

import './Pagination.css';

const Pagination = props => {
  const { meta, loading } = useSelector(state => ({ meta: state.meta, loading: state.loading }), shallowEqual);
  const currentPage = useMemo(() => meta.offset / RESULT_LIMIT + 1, [meta]);
  const dispatch = useDispatch();

  const handlePageChange = page => dispatch(fetchForPage(page));

  // no pagenation if data less than limit
  if (meta.total_count < meta.limit) 
    return null;

  return <AntPagination
    className={`pagination__container${props.className ? ` ${props.className}` : ''}`}
    current={currentPage}
    showSizeChanger={false}
    onChange={handlePageChange}
    // don't allow page selection while fetching data
    disabled={loading}
    total={meta.total_count} 
    { ...props.libraryProps } />
}

Pagination.propTypes = {
  className: PropTypes.string,
  libraryProps: PropTypes.object,
}

export default Pagination;