import React, { useMemo } from 'react';
import { shallowEqual, useSelector } from 'react-redux'
import { Table as AntTable } from 'antd';

import { COLUMN_KEY_VALUE } from '@constants'
import { CUSTOM_COLUMN_OPTIONS } from '@constants/ui'

const Table = () => {
  const { data, selectedColumns, loading } = useSelector(state => ({ 
    data: state.data.table, 
    selectedColumns: state.selectedColumns, 
    loading: state.loading,
  }), shallowEqual);
  
  const columns = useMemo(() => selectedColumns.map(column => ({
      title: COLUMN_KEY_VALUE[column],
      dataIndex: column,
      ...(CUSTOM_COLUMN_OPTIONS[column] || {})
    })), [selectedColumns]);

  let dataSource = data;

  if (selectedColumns.length === 0) {
    // get 'No Data' message when no columns selected
    dataSource = [];
  }
  
  return <>
    <AntTable
      dataSource={dataSource} 
      pagination={false}
      columns={columns} 
      loading={loading}
      bordered
      // scroll prop cause table re-render when available screen size change
      scroll={{ x: true }} 
      size="small"/>
  </>
}

export default Table;