import React, { useMemo } from 'react'
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux'
import { Spin } from 'antd';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'

import { COLUMNS } from '@constants';

import './Pie.css';

const defineOptions = (props, data) => ({
  title: { text: props.title },
  chart: { type: "pie" },
  plotOptions: {
    pie: {
      allowPointSelect: true,
      cursor: 'pointer',
      dataLabels: {
        enabled: true,
        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
      }
    }
  },
  series: [{ 
    name: props.hoverPointTitleText,
    data 
  }],
})

// memoize Pie to avoid re-renders on parent changes (e.g. column select/remove), more: https://react-redux.js.org/api/hooks#performance
const Pie = React.memo(props => {
  const { value, ...restProps } = props;
  const data = useSelector(state => Object.values(state.data.pie[value]));
  const options = useMemo(() => defineOptions(restProps, data), [data]);

  return <HighchartsReact highcharts={Highcharts} containerProps={{ className: 'pie__container' }} options={options} />
});

const MemoizedPieWithLoader = props => {
  const loading = useSelector(state => state.loading);
  
  return <Spin spinning={loading}>
    <Pie {...props} />
  </Spin>
}

MemoizedPieWithLoader.propTypes = {
  value: PropTypes.oneOf(COLUMNS),
  title: PropTypes.string,
  hoverPointTitleText: PropTypes.string
};

export default MemoizedPieWithLoader;