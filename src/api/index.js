import { API_URL, RESULT_LIMIT } from '@constants';

export const fetchData = options => {
  const offset = (options.page - 1) * RESULT_LIMIT;
  
  return fetch(`${API_URL}&limit=${RESULT_LIMIT}&offset=${offset}`)
    .then(response => response.json())
    .catch(error => ({ error }));
} 