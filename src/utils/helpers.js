/**
 * Normalize data for table & pies
 * 
 * Note: Currently only pie for protein_class is implemented.
 *    If you need to add another pie, add the respective 
 *    key to the initial value of the reduce below
 *    and write your logic. Then follow instruction
 *    from Pie component to initialize them
 *  
 * @param {*} data bioactivity data returned by API
 */
export const normalizeBioactivityData = data => {
  return data.reduce((accumulator, current, index) => {
    const proteinClass = current.protein_class || 'Not specified';

    // table data
    accumulator.table.push({
      key: index,
      ...current
    });

    // pie data
    if (!accumulator.pie.protein_class[proteinClass]) {
      accumulator.pie.protein_class[proteinClass] = {
        name: proteinClass,
        y: 0
      };
    }

    accumulator.pie.protein_class[proteinClass].y++;

    return accumulator;
  } , {
    // changes here, might also need to be applied @constants/initialState -> initialData object 
    table: [],
    pie: {
      protein_class: {},
    }
  });
}