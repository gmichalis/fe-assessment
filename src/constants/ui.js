/**
 * apply any of these option https://ant.design/components/table/#Column
 * to any of the columns needed (check column names @constants/index -> COLUMNS)
 **/ 
import React from 'react';
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';
import { Tag } from 'antd';

const COMPOUND_ID_URL = 'https://www.ebi.ac.uk/chembl/compound/inspect/';
const UNIPROT_ID = 'http://www.uniprot.org/uniprot/?query=accession:';

export const CUSTOM_COLUMN_OPTIONS = {
  annotated: {
    align: 'center',
    render: text => <>{ text === 'no' ? <CloseOutlined /> : <CheckOutlined />}</>,
    fixed: true,
  },
  chembl_id: {
    render: text => <a href={`${COMPOUND_ID_URL}${text}`} target="_blank">{text}</a>,
    fixed: true,
  },
  uniprot_id: {
    render: text => <a href={`${UNIPROT_ID}${text}`} target="_blank">{text}</a>,
    fixed: true,
  },
  endpoint_standard_type: {
    align: 'center',
    render: text => <Tag color="geekblue">{text}</Tag>
  },
  endpoint_standard_units: {
    align: 'center',
    render: text => <Tag color="green">{text}</Tag>
  }
}