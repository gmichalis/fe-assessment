import { COLUMNS, RESULT_LIMIT } from '.';

export const initialData = {
  table: [],
  pie: {
    protein_class: {},
  }
};

export const initialColumns = COLUMNS;

export const initialMeta = {
  limit: RESULT_LIMIT,
  next: null,
  offset: 0,
  previous: null,
  total_count: 0
};