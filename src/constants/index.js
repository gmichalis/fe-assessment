// API
export const API_URL = 'https://drugtargetcommons.fimm.fi/api/data/bioactivity/?format=json';
export const RESULT_LIMIT = 10;

/**
 * A core columns object that 
 * contains enough info to construct
 * the desired object/array by 
 * the view using it. 
 * 
 * Add your new [column]: [column_display_name] below
 * Sequence matters
 **/
export const COLUMN_KEY_VALUE = {
  annotated: 'Annotated',
  chembl_id: 'Compound ID',
  uniprot_id: 'Uniprot ID',
  activity_comment: 'Activity Comments',
  annotation_comments: 'Annotation Comments',
  assay_cell_type: 'Assay Cell Type',
  assay_type: 'Assay Type',
  assay_format: 'Assay Format',
  assay_sub_type: 'Assay Sub Type',
  authors: 'Authors',
  compound_concentration_value: 'Compound Concentration Value',
  compound_concentration_value_unit: 'Compound Concentration Value Unit',
  compound_name: 'Compound Name',
  detection_technology: 'Detection Technology',
  endpoint_actionmode: 'Endpoint Mode of Action',
  endpoint_standard_relation: 'End Point Standard Relation',
  endpoint_standard_type: 'End Point Standard Type',
  endpoint_standard_units: 'End Point Standard Units',
  endpoint_standard_value: 'End Point Standard Value',
  gene_name: 'Gene Name',
  inhibitor_type: 'Inhibitor Type',
  max_phase: 'Max Phase',
  mutation_info: 'Mutation Information',
  protein_class: 'Protein Class',
  pubmed_id: 'PubMed ID',
  resource_uri: 'Resource URI',
  target_organism: 'Target Organism',
  target_pref_name: 'Target Pref Name',
  wildtype_or_mutant: 'Wild Type or Mutant',
};

export const COLUMNS = Object.keys(COLUMN_KEY_VALUE);

export const COLUMN_VALUE_LABEL = COLUMNS.map(columnKey => ({ value: columnKey, label: COLUMN_KEY_VALUE[columnKey] }));