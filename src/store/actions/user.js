import { FETCH_DATA, COLUMN_MODIFICATION } from ".";

export const modifySelectedColumns = columns => ({ type: COLUMN_MODIFICATION, columns });

export const fetchForPage = page => ({ type: FETCH_DATA, page });