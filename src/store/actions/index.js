// misc
export const ERROR = 'ERROR';
export const DISMISS_ERROR = 'DISMISS_ERROR';

// column filter
export const COLUMN_MODIFICATION = 'COLUMN_MODIFICATION';

// data fetching
export const FETCH_DATA = 'FETCH_DATA';
export const DATA_FETCHED = 'DATA_FETCHED';