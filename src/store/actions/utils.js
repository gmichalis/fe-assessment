import { DISMISS_ERROR } from '.';

export const dismissError = () => ({ type: DISMISS_ERROR });