import { ERROR, DISMISS_ERROR, FETCH_DATA, DATA_FETCHED } from '@actions';
import { initialMeta } from '@constants/initialStates';

export const loadingReducer = (state = true, action) => {
  const { type } = action;

  switch (type) {
    case ERROR:
    case DATA_FETCHED:
      return false;
    case FETCH_DATA:
      return true;
    default:
      return state;
  }
};

export const errorReducer = (state = '', action) => {
  const { error, type } = action;

  switch (type) {
    case ERROR:
      return error;
    case DISMISS_ERROR:
      return '';
    default:
      return state;
  }
};

export const metaReducer = (state = initialMeta, action) => {
  const { meta, type } = action;

  switch (type) {
    case DATA_FETCHED:
      return meta;

    default:
      return state;
  }
}