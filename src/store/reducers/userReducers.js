import { COLUMN_MODIFICATION } from '@actions';
import { initialColumns } from '@constants/initialStates';

export const columnsReducer = (state = initialColumns, action) => {
  const { type, columns } = action;

  switch (type) {
    case COLUMN_MODIFICATION:
      return columns;
    default:
      return state;
  }
};
