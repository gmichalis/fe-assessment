import { DATA_FETCHED, FETCH_DATA } from '@actions';
import { initialData } from '@constants/initialStates';

export default (state = initialData, action) => {
  const { data, type } = action;

  switch (type) {
    case DATA_FETCHED:
      return data;

    default:
      return state;
  }
}