import { combineReducers } from 'redux';
import dataReducer from './dataReducer';
import { columnsReducer } from './userReducers';
import { loadingReducer, errorReducer, metaReducer } from './utilsReducers';

const reducers = combineReducers({
  // misc
  loading: loadingReducer,
  error: errorReducer,
  meta: metaReducer,

  // user
  selectedColumns: columnsReducer,
  
  // general
  data: dataReducer,
});

export default reducers;
