import { call, put, takeLatest } from 'redux-saga/effects';
import { FETCH_DATA, DATA_FETCHED, ERROR } from '@actions';
import * as api from '@api';

import { normalizeBioactivityData } from '@helpers';

function* fetchData(action) {
  const { page } = action;

  try {
    const data = yield call(api.fetchData, { page });

    if (data.bioactivities && data.bioactivities.length) {
      yield put({
        type: DATA_FETCHED,
        data: normalizeBioactivityData(data.bioactivities),
        meta: data.meta,
      });
    } else {
      throw data.error ? data.error.message : "Failed to fetch bioactivities";
    }
  } catch (error) {
    yield put({ type: ERROR, error });
  }
}

function* rootSaga() {
  yield fetchData({ page: 1 });
  yield takeLatest(FETCH_DATA, fetchData);
}

export default rootSaga;
