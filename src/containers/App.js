import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Button, Layout, notification } from 'antd';

import Table from '@components/Table';
import Pie from '@components/Pie';
import ColumnSelector from '@components/ColumnSelector';
import Pagination from '@components/Pagination';

import { dismissError } from '@actions/utils';

import './App.css';

const App = () => {
  const dispatch = useDispatch();
  const error = useSelector(state => state.error);

  useEffect(() => { 
    if (error) {
      notification.error({
        message: 'Error',
        description: error,
        placement: 'bottomRight',
        onClose: () => dispatch(dismissError())
      });
    }
  }, [error]);

  return <div className="app__container">
    <Layout.Sider
      className="app__sider"
      collapsedWidth={0}
      width={250}
      collapsible={true}
      breakpoint="lg"
      theme="light"
    >
      <ColumnSelector />
    </Layout.Sider>
    <div className="app__main">
      <Pie value="protein_class" title="Protein classes occurrences on current page" hoverPointTitleText="Elements in page" />
      <Table />
      <Pagination />
    </div>
  </div>;
}

export default App;