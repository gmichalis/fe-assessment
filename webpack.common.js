const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  entry: path.resolve('src/index'),
  optimization: {
    splitChunks: {
      chunks: 'all',
      name: false,
    },
    runtimeChunk: { // will allow a long term caching 
      name: entrypoint => `runtime-${entrypoint.name}`,
    },
  },
  resolve: {
    alias: {
      '@constants': path.resolve(__dirname, 'src/constants/'),
      '@components': path.resolve(__dirname, 'src/components/'),
      '@actions': path.resolve(__dirname, 'src/store/actions/'),
      '@api': path.resolve(__dirname, 'src/api/'),
      '@helpers': path.resolve(__dirname, 'src/utils/helpers'),
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        include: path.resolve('src'),
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            compact: true,
          }
        }
      },
    ]
  },
};