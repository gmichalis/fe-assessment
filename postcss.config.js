module.exports = {
  plugins: [
    require('postcss-preset-env')({
      autoprefixer: {
        // https://github.com/postcss/autoprefixer#features
        flexbox: 'no-2009'
      },
      stage: 3,
    })
  ]
}