# Front-end Engineering Assessment
Retrieve and display data provided by https://drugtargetcommons.fimm.fi

[[_TOC_]]

# Getting Started

## Prerequisites
- [Node.js](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/getting-started/install#global-install) (optional but recommended)

## Installing
Run `yarn` (or `npm install`)

## Running
Run `yarn start` (or `npm run start`)

# Credits
 - Webpack config inspiration and assistance from [create-react-app](https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/config/webpack.config.js)

# Wiki (can be moved to wiki section)

## General
  - Add or remove columns/options: `@constants/index` and edit `COLUMN_KEY_VALUE` 
  - Recucers initial state: `@constants/initialStates` and ecdit
  - Custom column [options](https://ant.design/components/table/#Column) (e.g. render link instead of text): `@constants/ui` and edit add/edit the key of the column you need to customize

## Components

- ### `<Pie />`
  Props:
  
  | name                | type   |
  |---------------------|--------|
  | value               | enum*  |
  | title               | string |
  | hoverPointTitleText | string |

  *One of column names from `COLUMN_KEY_VALUE` at `@constants/index`

  Example:
  ```jsx
    import Pie from '@components/Pie';

    // ...
    return <>
      {/* ... */}
      <Pie
        value="VALUE_NAME"
        title="Your title"
        hoverPointTitleText="Your point's title" />
    </>
    
  ```

- ### `<Pagination />`
  Props:

  | name         | type    |
  |--------------|---------|
  | className    | string  |
  | libraryProps | object* |

  *Library's pagination props can be found [here](https://ant.design/components/pagination/#API)

  Example:
    ```jsx
      import Pagination from '@components/Pagination';

      // ...
      return <>
        {/* ... */}
        <Pie className="custom-class" libraryProps={{ showSizeChanger: true }} />
      </>
    ```